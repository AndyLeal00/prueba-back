<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AwardsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::group(['prefix' => 'auth'], function () {
    Route::get('/', [LoginController::class, 'index'])->name('auth');
    Route::post('/login', [LoginController::class, 'login'])->name('auth.login');
    Route::get('/logout', [LoginController::class, 'logout'])->name('auth.logout');
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/create', [UserController::class, 'create'])->name('users.create');
    Route::post('/store', [UserController::class, 'store'])->name('users.store');
});

Route::group(['prefix' => 'awards'], function () {
    Route::get('/{award}', [AwardsController::class, 'edit'])->name('awards.edit');
    Route::post('/store', [AwardsController::class, 'store'])->name('awards.store');
    Route::delete('/destroy', [AwardsController::class, 'destroy'])->name('awards.destroy');
});

Route::middleware(['auth'])->group(function () { // Middleware de autenticación
    Route::group(['prefix' => 'awards'], function () {
        Route::get('/', [AwardsController::class, 'index'])->name('awards');
    });
});
