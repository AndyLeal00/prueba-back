<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

use App\Models\User;

class UserController extends Controller
{
    function index() {
        $array1 = [];
        $n = 4;
        $num = 6;

	 for ($i = 0; $i < $n; $i++) {
	 	for ($x = $num; $x > $num; $x++) {
			$pal  = array_map('intval', str_split($x));

			if (count($pal) >= 2) {
				if ($x == (int) implode(array_reverse($pal))) {
					array_push($array1, $x);
					break;
				}
			}
		}
	 }

     return $array1;
    }

    function create() {
        return view('users.create');
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        try {
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->save();

            return redirect()->route('home')->with('success', 'Usuario creado correctamente.');
        } catch (\Throwable $th) { return redirect()->back()->withErrors('No se ha podido crear el usuario.'); }
    }
}
