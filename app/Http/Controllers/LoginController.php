<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\auth;

class LoginController extends Controller {
    public function index() {
        return view('login');
    }

    public function login(Request $request) {
        try {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $request->session()->regenerate();
                return redirect()->intended(route('home'));
            } else { return redirect()->back()->withErrors('Contraseña o correo electrónico invalido.'); }
        } catch (\Throwable $th) { return redirect()->back()->withErrors('Ha ocurrido un error.'); }
    }

    public function logOut(Request $request) {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect(route('home'));
    }
}
