<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Award;

class AwardsController extends Controller {
    public function index() {
        $awards = Award::latest()->where('status', 1)->paginate(5);
        return view('awards.list', compact('awards'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required'
        ]);

        try {
            $data = Award::updateOrCreate([
                'id' => $request->award_id,
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone,
                'email' => $request->email
            ]);

            return response()->json(['success' => 'Premio guardado correctamente.', 'award' => $data]);
        } catch (\Throwable $th) { return redirect()->back()->withErrors('No se ha podido crear el premio.'); }
    }

    public function edit(Award $award) {
        $award = Award::find($id);
        return response()->json($award);
    }

    public function destroy(Request $request) {
        $request->validate([
            'id' => 'required'
        ]);

        Award::where('id', $request->id)->update(['status' => 0]);

        return response()->json(['success' => 'Premio eliminado.']);
    }
}
