@extends('layouts.app')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="row">
        <div class="d-flex justify-content-between col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Premios</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="javascript:void(0)" id="createNewAward"> Crear premio</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-striped mt-5">
        <thead>
            <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Dirección</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Correo electrónico</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody id="tableBody">
            @foreach ($awards as $award)
                <tr>
                    <td>{{ $award->name }}</td>
                    <td>{{ $award->address }}</td>
                    <td>{{ $award->phone }}</td>
                    <td>{{ $award->email }}</td>
                    <td>
                        <a href="javascript:void(0)" data-toggle="tooltip" data-id="{{ $award->id }}" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>
                        <a href="javascript:void(0)" data-toggle="tooltip" data-id="{{ $award->id }}" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {!! $awards->links() !!}

    <!-- Modal -->
    <div class="modal fade" id="ajaxModel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="modelHeading">Modal title</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display: none" id="div-errors">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul id="log-errors">
                        </ul>
                    </div>
                    <form id="awardForm" name="awardForm" class="form-horizontal">
                        @csrf
                        <input type="hidden" name="award_id" id="award_id">
                        <div class="mb-3">
                            <label for="name" class="col-form-label">Nombre:</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="mb-3">
                            <label for="address" class="col-form-label">Dirección:</label>
                            <input type="text" class="form-control" id="address" name="address" required>
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="col-form-label">Teléfono:</label>
                            <input type="number" class="form-control" id="phone" name="phone" max="10" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="col-form-label">Correo electrónico:</label>
                            <input type="text" class="form-control" id="email" name="email" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" type="submit" class="btn btn-primary" id="saveBtn"></button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            /*------------------------------------------
             --------------------------------------------
             Pass Header Token
             --------------------------------------------
             --------------------------------------------*/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            /*------------------------------------------
            --------------------------------------------
            Click to Button
            --------------------------------------------
            --------------------------------------------*/
            $('#createNewAward').click(function() {
                $('#log-errors').empty();
                $('#div-errors').hide();
                $('#saveBtn').text("Crear");
                $('#award_id').val('');
                $('#awardForm').trigger("reset");
                $('#modelHeading').html("Crear Premio");
                $('#ajaxModel').modal('show');
            });

            /*------------------------------------------
            --------------------------------------------
            Click to Edit Button
            --------------------------------------------
            --------------------------------------------*/
            $('body').on('click', '.editProduct', function() {
                var product_id = $(this).data('id');
                $.get("{{ route('awards') }}" + '/' + product_id + '/edit', function(
                    data) {
                    $('#modelHeading').html("Edit Product");
                    $('#saveBtn').val("edit-user");
                    $('#ajaxModel').modal('show');
                    $('#product_id').val(data.id);
                    $('#name').val(data.name);
                    $('#detail').val(data.detail);
                })
            });

            /*------------------------------------------
            --------------------------------------------
            Create Product Code
            --------------------------------------------
            --------------------------------------------*/
            $('#saveBtn').click(function(e) {
                e.preventDefault();
                $(this).html('Enviando..');

                $.ajax({
                    data: $('#awardForm').serialize(),
                    url: "{{ route('awards.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        $('#awardForm').trigger("reset");
                        $('#ajaxModel').modal('hide');

                        console.log(data);
                        let txt = `
                            <tr>
                                <td>${data.award.name}</td>
                                <td>${data.award.address}</td>
                                <td>${data.award.phone}</td>
                                <td>${data.award.email}</td>
                                <td>
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-id="${data.award.id}" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-id="${data.award.id}" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>
                                </td>
                            </tr>`;

                        $('#tableBody').append(txt);
                    },
                    error: function(data) {
                        console.log('Error:', data);

                        let txt = '';
                        let errors = Object.keys(data.responseJSON.errors).map((key) => [key, data.responseJSON.errors[key]]);
                        errors.map(resp => {
                            txt += `<li>${resp[1][0]}</li>`;
                        });

                        $('#log-errors').append(txt);
                        $('#div-errors').show();
                        $('#saveBtn').html('Guardar cambios');
                    }
                });
            });

            /*------------------------------------------
            --------------------------------------------
            Delete Product Code
            --------------------------------------------
            --------------------------------------------*/
            $('body').on('click', '.deleteProduct', function() {

                var product_id = $(this).data("id");
                confirm("Are You sure want to delete !");

                $.ajax({
                    type: "DELETE",
                    url: "{{ route('awards.destroy') }}" + '/' + product_id,
                    success: function(data) {
                        table.draw();
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            });

        });
    </script>
@endsection
