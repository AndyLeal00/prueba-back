@vite(['resources/js/app.js'])
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Laravel App</title>
</head>
<body>
    <header>
        @include('layouts.header')
    </header>

    <main class="p-4">
        @yield('content')
    </main>
</body>
</html>
