<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Nombre épico</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}"> Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('awards') }}">Premios</a>
                    </li>
                @endauth
            </ul>
            <div>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    @auth
                        <li class="nav-item">
                            <label class="nav-link">{{ Auth::user()->name }}</label>
                        </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('users.create') }}"> Sign up</a>
                            </li>
                    @endauth
                    <li class="nav-item">
                        @auth
                            <a class="nav-link" href="{{ route('auth.logout') }}"> Logout</a>
                            @else
                                <a class="nav-link" href="{{ route('auth') }}"> Login</a>
                        @endauth
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
